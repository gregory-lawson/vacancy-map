import { AirtableRecord } from "~/types"

const extractFields = <T>(records: AirtableRecord<T>[]): T[] => {
  return records.map(record => record.fields)
}

export default extractFields
