// Airtable
export type AirtableRecord<T> = { fields: T }

export type AirtableRecordsResponse<T> = {
  error?: unknown
  records?: AirtableRecord<T>[]
  offset?: string // used for pagination
}

// Vacant Spaces
export enum VACANT_SPACE_OWNER {
  PRIVATE = 'private',
  PUBLIC_FEDERAL = 'public-federal',
  PUBLIC_PROVINCIAL = 'public-provincial',
  PUBLIC_MUNICIPAL = 'public-municipal',
  UNKNOWN = 'unknown',
}

export enum VACANT_SPACE_STATUS {
  VACANT = 'vacant',
  SQUATTED = 'squatted',
  IN_USE = 'in use',
}

export interface VacantSpace {
  longitude: number
  latitude: number
  address?: string
  owner: VACANT_SPACE_OWNER
  status?: VACANT_SPACE_STATUS
  moreInfo?: string
  vacantSince?: string // iso datetime
  created?: string // iso datetime
}

